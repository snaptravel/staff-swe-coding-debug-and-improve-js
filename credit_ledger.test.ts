import { CreditDb } from './credit_db'
import { CreditLedger } from './credit_ledger'

describe('credit ledger tests', () => {
  let creditDbInst = new CreditDb()
  let ledger = new CreditLedger(creditDbInst)
  ledger.seed()

  test('get credits in 31 days', () => {
    let balance = ledger.getUserBalance('1', 31)
    expect(balance).toBe(5)
  });
});