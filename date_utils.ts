export function getCurrentDate() : Date {
  return new Date()
}

export function getDateNDaysIntoFuture(days: number) : Date {
  let future = new Date()
  future.setDate(future.getDate() + days)
  return future
}

export function dateToStr(date: Date) : null | string {
  if (date) {
    return date.toISOString()
  }
  return null
}