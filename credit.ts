import {getCurrentDate, dateToStr} from './date_utils'
import {v4 as uuidv4} from 'uuid';


export class Credit {
  userId: string
  amount: number
  createdAt: Date
  creditId: string
  expiresAt: null | Date

  constructor(
      userId: string,
      amount: number,
      createdAt: Date = null,
      expiresAt: Date = null) {
    this.userId = userId
    this.amount = amount
    this.createdAt = createdAt ||  getCurrentDate()
    this.creditId = uuidv4()
    this.expiresAt = expiresAt
  }

  toDict(): object {
    return {
      'creditId': this.creditId,
      'userId': this.userId,
      'amount': Math.round(this.amount * 100) / 100,
      'expiresAt': dateToStr(this.expiresAt),
      'createdAt': dateToStr(this.createdAt),
    } 
  }
     
}