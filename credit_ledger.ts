import {Credit} from './credit'
import {CreditDb} from './credit_db'
import {getDateNDaysIntoFuture} from './date_utils'


export class CreditLedger {
  db: CreditDb

  constructor(db_inst: CreditDb) {
    this.db = db_inst
  }

  addUserCredit(userId: string, amount: number, createdAt: Date = null, expiresAt: Date = null) : void {
    if (amount < 0) {
      let balance = this.getUserBalance(userId)
      if (balance + amount < 0) {
        throw Error('Not permitted - credits will drop below zero')
      } 
    }
    let newCredit = new Credit(userId, amount, createdAt=createdAt, expiresAt=expiresAt)
    this.db.save(newCredit)
  }

  getUserBalance(userId: string, inDays: number = 0) : number {
    let balance = 0
    let allCredits = Object.values(this.db.getAll())
    let atTime = getDateNDaysIntoFuture(inDays)
    for (const credit of allCredits) {
      if (credit.userId != userId) {
        continue
      }
      if (credit.expiresAt && credit.expiresAt <= atTime) {
        continue
      }
      balance += credit.amount
    }
    return balance
  }

  seed() : void {
    this.addUserCredit('1', 20, null, getDateNDaysIntoFuture(362))
    this.addUserCredit('1', -15, getDateNDaysIntoFuture(30), null)
  }
}
