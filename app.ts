import {CreditDb} from './credit_db'
import {CreditLedger} from './credit_ledger'

let creditDbInst = new CreditDb()
let creditLedger = new CreditLedger(creditDbInst)
creditLedger.seed()

console.log(`\n\n\n\nDb data:\n\n`)
console.log(creditDbInst.getAll())
console.log(`\n\n\n\nBalance in 1 year: ${creditLedger.getUserBalance('1', 365)}\n\n\n\n`)