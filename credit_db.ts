import {Credit} from './credit'
import clone from 'lodash.clone'


export class CreditDb {
  db: object

  constructor() {
    this.db = {}
  }

  clear() {
    this.db = {}
  }

  save(credit: Credit) : boolean {
    if (!credit || !credit.creditId) {
      return false
    }
    this.db[credit.creditId] = credit
    return true
  }

  get(creditId: string) : Credit | null {
    return this.db[creditId]
  }

  getAll() : object {
    return clone(this.db)
  }

  delete(creditId: string) : boolean {
    if (creditId in this.db) {
      delete this.db[creditId]
      return true
    }
    return false
  }
}
